package org.seferi.AI;

import org.seferi.GameState;

import java.io.IOException;
import java.util.ArrayList;

import static org.seferi.Main.makeUserMove;
import static org.seferi.Service.*;

public class LevelMedium implements AI{
    public static void getMove() {
        Move aiMove = new Move();
        ArrayList<Move> freeCoordinates = AI.getFreeCoordinates();

        //check two in a row
        if (gameState == GameState.GAME_NOT_FINISHED) {
            for (int i = 0; i < 3; i++) {
                if (field[i][0] + field[i][1] == 176 && field[i][2] == ' ' || field[i][0] + field[i][1] == 158 && field[i][2] == ' ') {
                    aiMove.row = i;
                    aiMove.col = 2;
                    break;
                } else if (field[i][0] + field[i][2] == 176 && field[i][1] == ' ' || field[i][0] + field[i][2] == 158 && field[i][1] == ' ') {
                    aiMove.row = i;
                    aiMove.col = 1;
                    break;
                } else if (field[i][1] + field[i][2] == 176 && field[i][0] == ' ' || field[i][1] + field[i][2] == 158 && field[i][0] == ' ') {
                    aiMove.row = i;
                    aiMove.col = 0;
                    break;
                } else if (field[0][i] + field[1][i] == 176 && field[2][i] == ' ' || field[0][i] + field[1][i] == 158 && field[2][i] == ' ') {
                    aiMove.row = 2;
                    aiMove.col = i;
                    break;
                } else if (field[0][i] + field[2][i] == 176 && field[1][i] == ' ' || field[0][i] + field[2][i] == 158 && field[1][i] == ' ') {
                    aiMove.row = 1;
                    aiMove.col = i;
                    break;
                } else if (field[1][i] + field[2][i] == 176 && field[0][i] == ' ' || field[1][i] + field[2][i] == 158 && field[0][i] == ' ') {
                    aiMove.row = 0;
                    aiMove.col = i;
                    break;
                } else if (field[0][0] + field[1][1] == 176 && field[2][2] == ' ' || field[0][0] + field[1][1] == 158 && field[2][2] == ' ') {
                    aiMove.row = 2;
                    aiMove.col = 2;
                    break;
                } else if (field[1][1] + field[2][2] == 176 && field[0][0] == ' ' || field[1][1] + field[2][2] == 158 && field[0][0] == ' ') {
                    aiMove.row = 0;
                    aiMove.col = 0;
                    break;
                } else if (field[0][0] + field[2][2] == 176 && field[1][1] == ' ' || field[0][0] + field[2][2] == 158 && field[1][1] == ' ') {
                    aiMove.row = 1;
                    aiMove.col = 1;
                    break;
                } else if (field[0][2] + field[1][1] == 176 && field[2][0] == ' ' || field[0][2] + field[1][1] == 158 && field[2][0] == ' ') {
                    aiMove.row = 2;
                    aiMove.col = 0;
                    break;
                } else if (field[1][1] + field[2][0] == 176 && field[0][2] == ' ' || field[1][1] + field[2][0] == 158 && field[0][2] == ' ') {
                    aiMove.row = 0;
                    aiMove.col = 2;
                    break;
                } else if (field[0][2] + field[2][0] == 176 && field[1][1] == ' ' || field[0][2] + field[2][0] == 158 && field[1][1] == ' ') {
                    aiMove.row = 1;
                    aiMove.col = 1;
                    break;
                } else if (freeCoordinates.size() != 0 && aiMove.equals(new Move())) {
                    int move = (int) (Math.random() * freeCoordinates.size());
                    aiMove = freeCoordinates.get(move);
                }
            }

            if (freeCoordinates.size() != 0) {

                System.out.println("Making move level \"medium\"");
                field[aiMove.row][aiMove.col] = playerTurn;
                displayGameField();

                gameState = analyzeCells();
            }

        }
    }

    public static void aiVSai() {
        while(gameState == GameState.GAME_NOT_FINISHED) {
            getMove();
            playerTurn = playerTurn == 'X' ? 'O' : 'X';
        }
    }

    public static void userVSai(boolean aiFirst) throws IOException {
        while (gameState == GameState.GAME_NOT_FINISHED) {
            if (!aiFirst) {
                gameState = analyzeCells();
                makeUserMove();
                playerTurn = playerTurn == 'X' ? 'O' : 'X';
                gameState = analyzeCells();
                getMove();
            } else {
                gameState = analyzeCells();
                getMove();
                playerTurn = playerTurn == 'X' ? 'O' : 'X';
                gameState = analyzeCells();
                makeUserMove();
            }
            playerTurn = playerTurn == 'X' ? 'O' : 'X';
        }
    }


}
