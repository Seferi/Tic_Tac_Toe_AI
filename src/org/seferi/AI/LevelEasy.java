package org.seferi.AI;

import org.seferi.GameState;

import java.io.IOException;
import java.util.ArrayList;

import static org.seferi.Main.makeUserMove;
import static org.seferi.Service.*;
import static org.seferi.Service.analyzeCells;

public class LevelEasy implements AI {



    public static void getMove() {
        ArrayList<Move> freeCoordinates = AI.getFreeCoordinates();
        int randomMove = (int) (Math.random() * freeCoordinates.size());
        Move move = freeCoordinates.get(randomMove);
        System.out.println("Making move level \"easy\"");
        field[move.col][move.row] = playerTurn;
        displayGameField();
        gameState = analyzeCells();
    }

    public static void aiVSai() {
        while(gameState == GameState.GAME_NOT_FINISHED) {
            getMove();
            playerTurn = playerTurn == 'X' ? 'O' : 'X';
        }
    }

    public static void userVSai(boolean aiFirst) throws IOException {
        while (gameState == GameState.GAME_NOT_FINISHED) {
            if (!aiFirst) {
                gameState = analyzeCells();
                makeUserMove();
                playerTurn = playerTurn == 'X' ? 'O' : 'X';
                gameState = analyzeCells();
                getMove();
            } else {
                gameState = analyzeCells();
                getMove();
                playerTurn = playerTurn == 'X' ? 'O' : 'X';
                gameState = analyzeCells();
                makeUserMove();
            }
            playerTurn = playerTurn == 'X' ? 'O' : 'X';
        }
    }


}
