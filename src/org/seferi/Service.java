package org.seferi;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Service {
    public static GameState gameState = GameState.GAME_NOT_FINISHED;
    public static char[][] field = new char[3][3];
    public static char playerTurn = 'X';

    public static void enterCells(char[] cells) throws IOException {

        int cell = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (cells[cell] == '_') {
                    field[i][j] = ' ';
                    cell++;
                } else {
                    field[i][j] = cells[cell];
                    cell++;
                }
            }
        }

    }

    public static boolean checkCoordinates(String coordinates) {
        String[] coordinatesSplit = coordinates.split(" ");
        String regex = "\\d+";

        for (String str : coordinatesSplit) {
            if (!str.matches(regex)) {
                System.out.println("You should enter numbers!");
                return false;
            } else if (coordinatesSplit.length < 2) {
                System.out.println("You should enter 2 coordinates!");
                return false;
            } else if (Integer.parseInt(str) > 3 || Integer.parseInt(str) <= 0) {
                System.out.println("Coordinates should be from 1 to 3!");
                return false;
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (field[Integer.parseInt(coordinatesSplit[0])-1][Integer.parseInt(coordinatesSplit[1])-1] != ' ') {
                    System.out.println("This cell is occupied! Choose another one!");
                    return false;
                }
            }
        }
        return true;
    }

    public static void displayGameField() {
        System.out.println("---------");
        for (int i = 0; i < 3; i++) {
            System.out.print("|");
            for (int j = 0; j < 3; j++) {
                System.out.print(" " + field[i][j]);
            }
            System.out.println(" |");
        }
        System.out.println("---------");
    }

    public static GameState analyzeCells() {
        boolean xWins = false;
        boolean oWins = false;
        boolean draw = false;
        boolean impossible = false;
        boolean gameNotFinished = false;
        int xCount = 0;
        int oCount = 0;
        int emptyCell = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (field[i][j] == 'X') {
                    xCount++;
                } else if (field[i][j] == 'O') {
                    oCount++;
                } else {
                    emptyCell++;
                }
            }
        }

        //Checks for the three in a row
        for (int i = 0; i < 3; i++) {
            int sumRows = 0;
            int sumColumn = 0;
            for (int j = 0; j < 3; j++) {
                sumRows += field[i][j];
                sumColumn += field[j][i];
            }
            if (sumRows == 264 || sumColumn == 264) {
                xWins = true;
            } else if (sumRows == 237 || sumColumn == 237) {
                oWins = true;
            }
        }

        if (field[0][0] + field[1][1] + field[2][2] == 264 || field[0][2] + field[1][1] + field[2][0] == 264) {
            xWins = true;
        }

        if (field[0][0] + field[1][1] + field[2][2] == 237 || field[0][2] + field[1][1] + field[2][0] == 237) {
            oWins = true;
        }

        if (xWins && oWins || Math.abs(xCount - oCount) > 1) {
            return GameState.IMPOSSIBLE;
        }

        if (!xWins && !oWins && emptyCell > 0) {
            return GameState.GAME_NOT_FINISHED;
        }

        if (xWins) {
            return GameState.X_WINS;
        }
        if (oWins) {
            return GameState.O_WINS;
        }
        return GameState.DRAW;
    }
}
